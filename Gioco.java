package com.gioco.sos.Gioco;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
public class Gioco extends Canvas implements Runnable, KeyListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int larghezza = 1920;
	private static final int altezza= 1080;
	private static final String nome = "prova";
	private boolean GiocoAttivo=false;
	BufferedImage sfondo= null;
	BufferedImage unicorno= null; 
	BufferedImage pandacorno= null; 
	private Unicorno ogg_unicorno;
	public Gioco() {
	CaricaRisorse();
	IniziaGioco();
	}
	
	public static void main (String[] args) {
		Gioco gioco = new Gioco();
		
	JFrame finestra_gioco = new	JFrame (nome); 
	Dimension dimensione_finestra = new Dimension (larghezza,altezza);
	finestra_gioco.setPreferredSize(dimensione_finestra);
	finestra_gioco.setMaximumSize(dimensione_finestra);
	finestra_gioco.setResizable(false);
	
	finestra_gioco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	finestra_gioco.add(gioco);
	finestra_gioco.addKeyListener(gioco);
	finestra_gioco.pack();
	finestra_gioco.setVisible(true);
	Thread thread_gioco = new Thread(gioco);
	thread_gioco.start();
	
	}
	private void IniziaGioco() {
	ogg_unicorno=new Unicorno(unicorno,150,75,100,600);
	ogg_unicorno.start();
	}
	
		
	
	private void CaricaRisorse() {
		Caricatore_immagini loader=new Caricatore_immagini();
		sfondo= loader.carica_immagine("/12526.jpg");
		unicorno= loader.carica_immagine("/unicorno.png");
		System.out.println("Risorse caricate!");
	}
	private void disegna() {
		Graphics g = this.getGraphics();
		g.drawImage(sfondo, 0, 0,larghezza,altezza,this );
		ogg_unicorno.disegna(g);
		g.dispose();
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() {
		GiocoAttivo=true;
		while(GiocoAttivo) {
			disegna();
			
		}
		
		
	}
	
}
